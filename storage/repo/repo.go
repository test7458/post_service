package repo

import (
	"errors"

	pb "gitlab.com/test/post_service/protos/post_service"
)

var (
	// ErrAlreadyExists ...
	ErrAlreadyExists = errors.New("already exists")
	// ErrInvalidField ...
	ErrInvalidField = errors.New("incorrect field")
)

// PostRepository is an interface for client storage
type PostRepository interface {
	Get(id uint64) (*pb.Post, error)
	List(limit, page uint64) (*pb.ListPostResponse, error)
	Create(*pb.Post) (*pb.Post, error)
	Update(*pb.Post) (*pb.Post, error)
	Delete(uint64) error
	CreateMultiple([]*pb.Post) ([]*pb.Post, error)
}
