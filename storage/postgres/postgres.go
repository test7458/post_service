package postgres

import (
	"database/sql"
	"errors"
	"fmt"
	"log"

	"github.com/jmoiron/sqlx"
	// "google.golang.org/protobuf/internal/errors"
	pb "gitlab.com/test/post_service/protos/post_service"

	"gitlab.com/test/post_service/platform/postgres"
	"gitlab.com/test/post_service/storage/repo"
)

// PostRepo ...
type postRepo struct {
	db *sqlx.DB
}

// NewPostRepo ...
func NewPostRepo() repo.PostRepository {
	return &postRepo{
		db: postgres.DB(),
	}
}

func (r *postRepo) CreateMultiple(posts []*pb.Post) ([]*pb.Post, error) {

	tx, err := r.db.Begin()
	defer func() {
		err := tx.Commit()
		if err != nil {
			tx.Rollback()
		}
	}()

	if err != nil {
		log.Println("Error beginning transaction")
		return nil, err
	}
	qry := `INSERT INTO 
		posts  (id, title, body, creator_id) 
		VALUES `

	for _, post := range posts {
		qry += fmt.Sprintf("(%d, '%s', '%s', %d), ", post.Id, post.Title, post.Body, post.UserId)
	}

	qry = qry[:len(qry)-2] // trims thee last ", "
	_, err = tx.Exec(qry)

	if err != nil {
		tx.Rollback()
		return nil, err
	}

	return posts, nil
}

func (r *postRepo) Create(p *pb.Post) (*pb.Post, error) {

	tx, err := r.db.Begin()
	defer func() {
		err := tx.Commit()
		if err != nil {
			tx.Rollback()
		}
	}()

	if err != nil {
		log.Println("Error beginning transaction")
		return nil, err
	}
	err = tx.QueryRow(`
		INSERT INTO 
		posts  (title, body, creator_id) 
		VALUES ($1, $2, $3) RETURNING id
	`, p.GetTitle(), p.GetBody(), p.GetUserId()).Scan(&p.Id)

	if err != nil {
		tx.Rollback()
		return nil, err
	}

	return p, nil
}

func (r *postRepo) Update(p *pb.Post) (*pb.Post, error) {
	tx, err := r.db.Begin()
	defer func() {
		err := tx.Commit()
		if err != nil {
			tx.Rollback()
		}
	}()

	if err != nil {
		log.Println("Error beginning transaction")
		return nil, err
	}
	res, err := tx.Exec(`
		UPDATE posts 
		SET title = (case when $2!='' and $2 IS NOT NULL then $2 else title end),
		body = (case when $3!='' and $3 IS NOT NULL then $3	else body end),
		updated_at=CURRENT_TIMESTAMP
		WHERE id=$1 AND creator_id=$4 AND deleted_at IS NULL
	`, p.GetId(), p.GetTitle(), p.GetBody(), p.GetUserId())

	if err != nil {
		tx.Rollback()
		return nil, err
	}

	if rows, _ := res.RowsAffected(); rows == 0 {
		return nil, sql.ErrNoRows
	}

	return nil, nil
}

func (r *postRepo) Get(id uint64) (*pb.Post, error) {
	post := pb.Post{}

	err := r.db.QueryRow(`
	SELECT id, title, body, creator_id
	FROM posts 
	WHERE id = $1 AND deleted_at IS NULL
	`, id).Scan(&post.Id, &post.Title, &post.Body, &post.UserId)
	if err != nil {
		return nil, err
	}
	return &post, nil
}

func (r *postRepo) Delete(id uint64) error {
	tx, err := r.db.Begin()
	defer func() {
		err := tx.Commit()
		if err != nil {
			tx.Rollback()
		}
	}()

	if err != nil {
		log.Println("Error beginning transaction")
		return err
	}
	res, err := tx.Exec(`
		UPDATE posts 
		SET deleted_at=CURRENT_TIMESTAMP
		WHERE id=$1
	`, id)

	if err != nil {
		tx.Rollback()
		return err
	}

	if rows, _ := res.RowsAffected(); rows == 0 {
		return sql.ErrNoRows
	}
	return nil
}

func (r *postRepo) List(limit, page uint64) (*pb.ListPostResponse, error) {

	res := pb.ListPostResponse{}
	res.Results = make([]*pb.Post, 0, limit)
	var count uint64

	if page < 1 {
		return nil, errors.New("Invalid value for page")
	}
	offset := (page - 1) * limit

	rows, err := r.db.Query(`
	SELECT id, title, body, creator_id, (SELECT count(id) FROM posts WHERE deleted_at IS NULL)
	FROM posts
	WHERE deleted_at IS NULL 
	LIMIT $1
	OFFSET $2`, limit, offset)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		post := pb.Post{}

		err := rows.Scan(&post.Id, &post.Title, &post.Body, &post.UserId, &count)
		if err != nil {
			return nil, err
		}

		res.Results = append(res.Results, &post)
	}

	if err != nil {
		return nil, err
	}

	res.Count = count

	return &res, nil
}
