package storage

import (
	"gitlab.com/test/post_service/storage/postgres"
	"gitlab.com/test/post_service/storage/repo"
)

type Storage struct {
	Repo repo.PostRepository
}

func NewStorage() Storage {

	return Storage{
		Repo: postgres.NewPostRepo(),
	}
}
