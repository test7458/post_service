CREATE TABLE posts (
    id SERIAL PRIMARY KEY,
    title VARCHAR(256) NOT NULL,
    body TEXT NOT NULL,
    creator_id INTEGER NOT NULL,
    created_at TIMESTAMP default CURRENT_TIMESTAMP,
    updated_at TIMESTAMP default NULL,
    deleted_at TIMESTAMP default NULL
);