package service

import (
	"context"
	"database/sql"
	"log"

	pb "gitlab.com/test/post_service/protos/post_service"
	"gitlab.com/test/post_service/storage"
	"go.uber.org/zap"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type postService struct {
	storage storage.Storage
	logger  *log.Logger
}

func NewPostServiceServer(l *log.Logger) pb.PostServiceServer {
	newPostService := postService{storage: storage.NewStorage(), logger: l}

	return &newPostService
}

func (s *postService) CreateMultiple(c context.Context, req *pb.CreateMultiplePostRequest) (
	*pb.CreateMultiplePostResponse, error) {

	posts, err := s.storage.Repo.CreateMultiple(req.GetPosts())
	if err != nil {
		s.logger.Println("Eroor in creating post", zap.Error(err))
		return nil, err
	}
	return &pb.CreateMultiplePostResponse{Posts: posts}, err
}

func (s *postService) Create(c context.Context, req *pb.CreatePostRequest) (
	*pb.CreatePostResponse, error) {

	post, err := s.storage.Repo.Create(req.GetPost())
	if err != nil {
		s.logger.Println("Eroor in creating post", zap.Error(err))
		return nil, err
	}
	return &pb.CreatePostResponse{Post: post}, err
}

func (s *postService) Get(ctx context.Context, req *pb.GetPostRequest) (
	*pb.GetPostResponse, error) {

	post, err := s.storage.Repo.Get(req.GetId())

	if err == sql.ErrNoRows {
		s.logger.Println("Eroor in gettung post: NOT FOUND", zap.Error(err))
		return nil, status.Error(codes.NotFound, "failed to get post")
	} else if err != nil {
		s.logger.Println("Eroor in gettung post", zap.Error(err))
		return nil, status.Error(codes.Internal, "failed to get post")
	}
	return &pb.GetPostResponse{Post: post}, err
}

func (s *postService) Update(ctx context.Context, req *pb.UpdatePostRequest) (
	*pb.UpdatePostResponse, error) {

	post, err := s.storage.Repo.Update(req.GetPost())
	if err == sql.ErrNoRows {
		s.logger.Println("Eroor in updating post: NOT FOUND", zap.Error(err))
		return nil, status.Error(codes.NotFound, "failed to update post")
	} else if err != nil {
		s.logger.Println("Eroor in updating post", zap.Error(err))
		return nil, status.Error(codes.Internal, "failed to update post")
	}
	return &pb.UpdatePostResponse{Post: post}, err
}

func (s *postService) Delete(ctx context.Context, req *pb.DeletePostRequest) (
	*pb.DeletePostResponse, error) {

	err := s.storage.Repo.Delete(req.GetId())
	if err == sql.ErrNoRows {
		s.logger.Println("Eroor in updating post: NOT FOUND", zap.Error(err))
		return nil, status.Error(codes.NotFound, "failed to delete post")
	} else if err != nil {
		s.logger.Println("Eroor in deleting post", zap.Error(err))
		return nil, status.Error(codes.Internal, "failed to delete post")
	}
	return &pb.DeletePostResponse{Id: req.GetId()}, err
}

func (s *postService) List(ctx context.Context, req *pb.ListPostRequest) (
	*pb.ListPostResponse, error) {

	res, err := s.storage.Repo.List(req.GetLimit(), req.GetPage())
	if err != nil {
		s.logger.Println("Eroor in updating post", zap.Error(err))
		return nil, err
	}
	return res, nil
}
