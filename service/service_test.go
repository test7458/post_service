package service_test

import (
	"context"
	"fmt"
	"log"
	"os"
	"testing"

	"github.com/joho/godotenv"
	"github.com/stretchr/testify/assert"

	pb "gitlab.com/test/post_service/protos/post_service"
	post_service "gitlab.com/test/post_service/protos/post_service"
	"gitlab.com/test/post_service/service"
)

var postServiceTest post_service.PostServiceServer

func init() {
	const path = ".env"
	if info, err := os.Stat(path); !os.IsNotExist(err) {
		if !info.IsDir() {
			godotenv.Load(path)
			if err != nil {
				fmt.Println("Err:", err)
			}
		}
	} else {
		fmt.Println("Not exists")
	}
	log := log.Default()

	log.SetPrefix("post_service_test: ")
	postServiceTest = service.NewPostServiceServer(log)
}
func TestCreatePost(t *testing.T) {

	testCases := make([]*pb.CreatePostRequest, 100)

	for i := 0; i < 100; i++ {
		testCases[i] = &post_service.CreatePostRequest{
			Post: &post_service.Post{
				Title:  fmt.Sprintf("title number:%d", i),
				Body:   fmt.Sprintf("body number:%d", i),
				UserId: 2758,
			},
		}
	}

	for _, testCase := range testCases {
		t.Run("Create posst", func(t *testing.T) {

			_, err := postServiceTest.Create(context.Background(), testCase)
			assert.Equal(t, nil, err, "Error in creating post")

		})
	}

}

func TestCreateMultiplePost(t *testing.T) {

	posts := make([]*pb.Post, 100)

	for i := 0; i < 100; i++ {
		posts[i] = &post_service.Post{
			Id:     uint64(10000 + i),
			Title:  fmt.Sprintf("TITLEnumber:%d", i),
			Body:   fmt.Sprintf("BODY number:%d", i),
			UserId: 9998,
		}
	}

	t.Run("Create posst", func(t *testing.T) {

		_, err := postServiceTest.CreateMultiple(context.Background(), &post_service.CreateMultiplePostRequest{
			Posts: posts,
		})
		assert.Equal(t, nil, err, "Error in creating post")

	})

}
func TestDeletePost(t *testing.T) {

	testCases := make([]*pb.DeletePostRequest, 100)

	for i := 0; i < 1; i++ {
		testCases[i] = &post_service.DeletePostRequest{
			Id: uint64(i),
		}
	}

	for _, testCase := range testCases {
		t.Run("delete post", func(t *testing.T) {

			_, err := postServiceTest.Delete(context.Background(), testCase)

			assert.Equal(t, nil, err, "Error in deleting post")

		})
	}

}

func TestGetPost(t *testing.T) {

	testCases := make([]*pb.GetPostRequest, 1)

	for i := 0; i < 1; i++ {
		testCases[i] = &post_service.GetPostRequest{
			Id: 104,
		}
	}

	for _, testCase := range testCases {
		t.Run("get post", func(t *testing.T) {

			res, err := postServiceTest.Get(context.Background(), testCase)

			t.Log("res:", res)
			assert.Equal(t, nil, err, "Error in getting post")

		})
	}

}

func TestUpdatePost(t *testing.T) {

	testCases := make([]*pb.UpdatePostRequest, 1)

	for i := 0; i < 1; i++ {
		testCases[i] = &post_service.UpdatePostRequest{
			Post: &post_service.Post{
				Id:    104,
				Title: "",
				Body:  "adsfjk",
			},
		}
	}

	for _, testCase := range testCases {
		t.Run("update post", func(t *testing.T) {

			res, err := postServiceTest.Update(context.Background(), testCase)

			t.Log("res:", res)
			assert.Equal(t, nil, err, "Error in updating post")

		})
	}

}

func TestListPost(t *testing.T) {

	testCases := make([]*pb.ListPostRequest, 10, 10)

	for i := 0; i < 10; i++ {
		testCases[i] = &post_service.ListPostRequest{
			Limit: 10,
			Page:  uint64(i) + 1}
	}

	for _, testCase := range testCases {
		t.Run("get post", func(t *testing.T) {

			res, err := postServiceTest.List(context.Background(), testCase)

			t.Log("res:", res)
			assert.Equal(t, nil, err, "Error in getting post")

		})
	}

}
