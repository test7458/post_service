package main

import (
	"fmt"
	"net"

	"log"

	pb "gitlab.com/test/post_service/protos/post_service"

	"gitlab.com/test/post_service/service"

	"go.uber.org/zap"

	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	_ "github.com/joho/godotenv/autoload" // load .env file automatically
	_ "github.com/joho/godotenv/autoload" // load .env file automatically

	"google.golang.org/grpc"

	"gitlab.com/test/post_service/config"

	_ "github.com/lib/pq"
)

func main() {

	cfg := config.Get()
	log := log.Default()

	log.SetPrefix("post_service: ")
	// =========================================================================
	// Migrations
	dbURL := fmt.Sprintf("postgres://%s:%s@%s:%d/%s?sslmode=disable", cfg.PostgresUser, cfg.PostgresPassword, cfg.PostgresHost, cfg.PostgresPort, cfg.PostgresDatabase)
	m, err := migrate.New("file://migrations", dbURL)
	if err != nil {
		log.Fatal("error in creating migrations: ", zap.Error(err))
	}
	if err := m.Up(); err != nil {
		log.Println("error updating migrations: ", zap.Error(err))
	}

	// =========================================================================
	listen, err := net.Listen("tcp", cfg.RPCPort)
	if err != nil {
		log.Fatalf("error listening tcp port %v", zap.Error(err))
	}

	server := grpc.NewServer()
	postServiceServer := service.NewPostServiceServer(log)
	pb.RegisterPostServiceServer(server, postServiceServer)

	log.Println("main: server running on port:", cfg.RPCPort)

	if err := server.Serve(listen); err != nil {
		log.Fatalf("error listening: %v", zap.Error(err))
	}

}
